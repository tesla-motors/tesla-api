# Tesla Motors API 

## Description

Interacts with the Tesla Motors API

## Installation 

```sh
$ npm install --save teslamotors-api
```

## Example

```javascript
import {TeslaApi, RestService} from 'teslamotors-api';
import {map, finish} from 'mumba-stream';

let UrlOptions = {
    protocol: 'https',
    slashes: true,
    hostname: 'owner-api.teslamotors.com',
    port: '443' 
};

let TeslaApiOptions = {
    query: {}
};

let restService = new RestService(UrlOptions);
let api = new TeslaApi(TeslaApiOptions, restService);
```

## Tests

To run the test suite, first install the dependencies, then run `npm test`:

```sh
$ npm install
$ npm test
```

## License

[Apache 2.0](LICENSE.txt)

* * *

&copy; 2016 Greg Keys. All rights reserved.

