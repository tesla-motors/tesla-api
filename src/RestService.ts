/**
 *
 * @copyright Greg Keys 2016. All rights reserved.
 * @license   Apache-2.0
 */

import request = require('request');
const url = require('url');

export interface UrlOptions {
	protocol: string,
	slashes: boolean,
	hostname: string,
	port: string
}

export class RestService {
	/**
	 * @param {UrlOptions} options
	 */
	constructor(private options: UrlOptions) {
		if (!options) {
			throw new Error('RestService: <options> required.');
		}

		if (!options.protocol) {
			throw new Error('RestService: <options.protocol> required.');
		}

		if (!options.slashes) {
			throw new Error('RestService: <options.slashes> required.');
		}

		if (!options.hostname) {
			throw new Error('RestService: <options.hostname> required.');
		}

		if (!options.port) {
			throw new Error('RestService: <options.port> required.');
		}
	}

	/**
	 * Gets data from a url
	 *
	 * @param {string | urlObject} url, converts urlObject into an href
	 * @returns {Promise<string>}
	 */
	public get(url: any): Promise<any> {
		if ((typeof url === 'object')) {
			url = this.getUrl(url);
		}

		return new Promise((resolve, reject) => {
			request.get(url, (error: any, response: any, body: string) => {
				if (error) {
					return reject(error);
				}

				if (response.statusCode != 200) {
					return reject(new Error('RestService.get statusCode ' + response.statusCode))
				}

				return resolve(body);
			});
		});
	}

	/**
	 * Combines constructor urlObject with callee's urlObject then Converts into a href
	 *
	 * @param {UrlOptions} urlObject
	 * @returns {string}
	 */
	private getUrl(urlObject?: UrlOptions) {
		if (!urlObject) {
			return url.format(this.options);
		}

		let urlObj = Object.assign({}, this.options, urlObject);

		return url.format(urlObj);
	}

	public post(url: any, data: any): Promise<any> {
		if ((typeof url === 'object')) {
			url = this.getUrl(url);
		}

		return new Promise((resolve, reject) => {
			request.post(url, data, (error: any, response: any, body: string) => {
				if (error) {
					return reject(error);
				}

				if (response.statusCode != 200) {
					return reject(new Error('RestService.get statusCode ' + response.statusCode))
				}

				return resolve(body);
			});
		});
	}
}
