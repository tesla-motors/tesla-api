
/**
 *
 * @copyright 2016 Greg Keys. All rights reserved.
 * @license   Apache-2.0
 */
import {RestService} from './RestService';

export interface TeslaApiOptions{
	query?: {}
}

export class TeslaApi {
	/**
	 *
	 * @param {TeslaApiOptions} options
	 * @param {RestService}   restService
	 */
	constructor(private options: TeslaApiOptions, private restService: RestService){
		if (!options) {
			throw new Error('TeslaApi: <options> required.');
		}

		if (!restService) {
			throw new Error('TeslaApi: <restService> required.');
		}
	}

}
