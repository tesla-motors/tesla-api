/**
 *
 * @copyright Greg Keys 2016. All rights reserved.
 * @license   Apache-2.0
 */
import {RestService} from "./RestService";

export interface accessTokenOptions {
    grant_type: string,
    client_id: string,
    client_secret: string,
    email: string,
    password: string
}

export class TeslaAuthApi {

    constructor(private options: accessTokenOptions, private restService: RestService) {
        if (!options) {
            throw new Error('TeslaAuthApi: <options> required.');
        }

        if (!restService) {
            throw new Error('TeslaAuthApi: <restService> required.');
        }
    }

    public getAccessToken(email: string, password: string) {
        let urlObject = {
            pathname: '/oauth/token'
        };

        let data = {
            form: {
                grant_type: "password",
                client_id: "e4a9949fcfa04068f59abb5a658f2bac0a3428e4652315490b659d5ab3f35a9e",
                client_secret: "c75f14bbadc8bee3a7594412c31416f8300256d7668ea7e6e7f06727bfb9d220",
                email: email,
                password: password
            }
        };

        return this.restService.post(urlObject, data).then((result) => {
            return JSON.parse(result);
        });
    }
}