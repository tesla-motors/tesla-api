/**
 * Remember to observe the 4 A's of testing and try to limit tests to just four calls:
 * - Arrange    - set up the system state
 * - Act        - do the thing we are testing
 * - Assert     - inspect the resulting state
 * - Annihilate - tear down
 *
 * @copyright 2016 Greg Keys. All rights reserved.
 * @license   Apache-2.0
 */

import * as assert from 'assert';
import * as nock from 'nock';
import {RestService, UrlOptions} from '../../src/RestService';

describe('RestService Unit Tests', () => {
    let instance: RestService;
    let options: UrlOptions;

    beforeEach(() => {
        options = {
            protocol: 'https',
            slashes: true,
            hostname: 'owner-api.teslamotors.com',
            port: '443'
        };

        instance = new RestService(options);
    });

    describe('RestService Instantiation Tests', () => {
        it('should throw if options missing', () => {
            assert.throws(() => {
                instance = new RestService(void 0);
            }, /<options> required/)
        });

        it('should throw if options.api.protocol is missing', () => {
            delete options.protocol;

            assert.throws(() => {
                instance = new RestService(options);
            }, /<options.protocol> required/)
        });

        it('should throw if options.slashes is missing', () => {
            delete options.slashes;

            assert.throws(() => {
                instance = new RestService(options);
            }, /<options.slashes> required/)
        });

        it('should throw if options.hostname is missing', () => {
            delete options.hostname;

            assert.throws(() => {
                instance = new RestService(options);
            }, /<options.hostname> required/)
        });

        it('should throw if options.port is missing', () => {
            delete options.port;

            assert.throws(() => {
                instance = new RestService(options);
            }, /<options.port> required/)
        });
    });

    describe('RestService GET Unit Tests', () => {
        it('should get some data from a string url', () => {
            //We know its calling the right url if its getting the right user info
            let host = 'https://owner-api.teslamotors.com';
            let path = '/api/1/vehicles/1/mobile_enabled';
            nock(host).get(path).reply(200, {"response": true});

            return instance.get(host + path)
                .then((result) => {
                    assert.equal(result, '{"response":true}');
                });
        });

        it('should get some data from a urlObject', () => {
            //We know its calling the right url if its getting the right user info
            let host = 'https://owner-api.teslamotors.com';
            let path = '/api/1/vehicles/1/mobile_enabled';
            let urlObject = {
                pathname: '/api/1/vehicles/1/mobile_enabled',
                query: {}
            };

            nock(host).get(path).reply(200, {"response": true});

            return instance.get(urlObject)
                .then((result) => {
                    assert.equal(result, '{"response":true}');
                });
        });

        //send to unknown url, one dns look up cant find
        it('should reject on bad request', () => {
            let host = 'https://owner-api.teslamotors.com';
            let path = '/api/1/vehicles/1/mobile_enabled';
            nock(host).get(path).replyWithError('Bad Request');

            return instance.get(host + path)
                .catch((reason: Error) => {
                    assert.equal(reason.message, 'Bad Request', 'expected a Bad Request');
                });
        });

        //get statusCode from response.statusCode
        it('should reject on non 200 status code', () => {
            let host = 'https://owner-api.teslamotors.com';
            let path = '/api/1/vehicles/1/mobile_enabled';
            nock(host).get(path).reply(400, 'foo');

            return instance.get(host + path)
                .catch((reason: Error) => {
                    assert.equal(reason.message, 'RestService.get statusCode 400');
                });
        });
    });

    describe('RestService POST Unit Tests', () => {
        it('should POST data to a url', () => {
            //We know its calling the right url if its getting the right user info
            let host = 'https://owner-api.teslamotors.com';
            let path = '/oauth/token';

            let data = {
                form: {
                    grant_type: "password",
                    client_id: "e4a9949fcfa04068f59abb5a658f2bac0a3428e4652315490b659d5ab3f35a9e",
                    client_secret: "c75f14bbadc8bee3a7594412c31416f8300256d7668ea7e6e7f06727bfb9d220",
                    email: "elon@teslamotors.com",
                    password: "edisonsux"
                }
            };

            let response = {
                "access_token": "abc123",
                "token_type": "bearer",
                "expires_in": 7776000,
                "created_at": 1457385291,
                "refresh_token": "cba321"
            };

            nock(host).post(path, data.form).reply(200, response);

            return instance.post(host + path, data)
                .then((result) => {
                    assert.equal(result, JSON.stringify(response));
                });
        })
    })
});
