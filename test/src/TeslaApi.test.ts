/**
 * Remember to observe the 4 A's of testing and try to limit tests to just four calls:
 * - Arrange    - set up the system state
 * - Act        - do the thing we are testing
 * - Assert     - inspect the resulting state
 * - Annihilate - tear down
 *
 * @copyright 2016 Greg Keys. All rights reserved.
 * @license   Apache-2.0
 */

import * as assert from 'assert';
import {TeslaApi, TeslaApiOptions} from '../../src/TeslaApi';
import {RestService} from '../../src/RestService';

describe('TeslaApi Unit Tests', () => {
	let instance: TeslaApi;
	let options: TeslaApiOptions;
	let restService: RestService;

	beforeEach(() => {
		options = {
			query: {}
		};

		restService = Object.create(RestService.prototype);
		instance = new TeslaApi(options, restService);
	});

	it('should throw if options missing', () => {
		assert.throws(() => {
			instance = new TeslaApi(void 0, restService);
		}, /<options> required/);
	});

	it('should throw if restService missing', () => {
		assert.throws(() => {
			instance = new TeslaApi(options, void 0);
		}, /<restService> required/);
	});

/*	it('should get a new userApi instance', () => {
		options = {
			query: {
				DBASE: 'DEV'
			}
		};

		restService = Object.create(RestService.prototype);
		instance = new TeslaApi(options, restService);
		assert((instance.userApi() instanceof D3UserApi));
	});*/
});
