/**
 * Remember to observe the 4 A's of testing and try to limit tests to just four calls:
 * - Arrange    - set up the system state
 * - Act        - do the thing we are testing
 * - Assert     - inspect the resulting state
 * - Annihilate - tear down
 *
 * @copyright 2016 Greg Keys. All rights reserved.
 * @license   Apache-2.0
 */
import * as assert from 'assert';
import {TeslaAuthApi, accessTokenOptions} from "../../src/TeslaAuthApi";
import {RestService} from "../../src/RestService";

describe('TeslaAuthApi Unit Tests', () => {
    let instance: TeslaAuthApi;
    let options: accessTokenOptions;
    let restService: RestService;


    beforeEach(() => {
        options = {
            grant_type: "password",
            client_id: "e4a9949fcfa04068f59abb5a658f2bac0a3428e4652315490b659d5ab3f35a9e",
            client_secret: "c75f14bbadc8bee3a7594412c31416f8300256d7668ea7e6e7f06727bfb9d220",
            email: "elon@teslamotors.com",
            password: "edisonsux"
        };

        restService = Object.create(RestService.prototype);
        instance = new TeslaAuthApi(options, restService);
    });

    it('should throw if options missing', () => {
        assert.throws(() => {
            instance = new TeslaAuthApi(void 0, restService);
        }, /<options> required/);
    });

    it('should throw if restService missing', () => {
        assert.throws(() => {
            instance = new TeslaAuthApi(options, void 0);
        }, /<restService> required/);
    });

    it('should set an access token', (done) => {
        restService.post = function () {
            return Promise.resolve('{"access_token": "abc123", "token_type": "bearer", "expires_in": 7776000, "created_at": 1457385291, "refresh_token": "cba321"}');
        };

        instance.getAccessToken("elon@teslamotors.com", "edisonsux")
            .then((result: any) => {
                assert.equal(result.access_token, "abc123", 'should be abc123');
                done();
            })
            .catch(console.error);
    });
});